
const sidebar = document.getElementById('sidebar');
const result = document.getElementById('result');
let classifier;
let video;
let flippedVideo;

const modelReady = () => {
  console.log('MODEL IS READY!')
};

const preload = () => {
  classifier = ml5.imageClassifier("https://storage.googleapis.com/tm-model/BlP1OZ89F/model.json", modelReady);
};

const setup = () => {
  createCanvas(1024, 728);
  video = createCapture(VIDEO);
  video.size(1024, 728);
  video.hide();

  flippedVideo = ml5.flipImage(video)
  classifyVideo();
};

const draw = () => {
  flippedVideo.remove();
  background(0);
  image(flippedVideo, 0, 0);
};

const classifyVideo = () => {
  flippedVideo = ml5.flipImage(video)
  classifier.classify(flippedVideo, gotResult);
};

const gotResult = (error, results) => {
  switch(results[0].label) {
    case 'Wrong: No Mask':
      result.style.backgroundColor = 'red';
      result.style.color = 'white';
      break;
    case 'Correct: Mask On':
      result.style.backgroundColor = 'green';
      break;
    default:
      break;
  };

  result.innerHTML = results[0].label;

  classifyVideo();
};